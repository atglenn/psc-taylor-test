<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/root">
    <xsl:choose>

      <!-- test if there is an entry -->
      <xsl:when test="entry">

        <xsl:for-each select="entry">

          <section id="estimate-results" class="estimate-results">
            <h2>Your Estimated Cost of Attendance for the 2018 - 2019 Academic Year</h2>
            <blockquote class="important">
              <strong>IMPORTANT NOTE:</strong> The budget amounts listed below have been compiled to convey approximate West Virginia University Keyser Campus charges by major for prospective students. These amounts are only estimates and are subject to change. They should not be construed as official costs but are for informational purposes only. If you would like an estimate that includes possible financial aid please <a href="http://financialaid.wvu.edu/net-price-calculator">use the Net Price Calculator</a>.
            </blockquote>
            <div class="wrap-table">
              <div class="est-table estimate-table responsive" id="estimate-table" >
                <xsl:variable name="estimateTableSubmissions">
                  <xsl:value-of select="translations/level" />, <xsl:value-of select="translations/semester" />, <xsl:value-of select="translations/residency" /> and <xsl:value-of select="major/title" />
                </xsl:variable>
                <div class="est-caption table-bg__blue-dark">Tuition and Fees<br /><xsl:value-of select="$estimateTableSubmissions" /></div>
                <div class="est-thead table-bg__blue-medium">
                  <div class="est-tr">
                    <div class="est-caption-td est-inner-border">&#160;<br />&#160;</div>
                    <div class="est-caption-th est-inner-border">Cost Per Credit <br />(1 credit hr)</div>
                    <div class="est-caption-th est-inner-border">Cost Per Semester <br />(min. 12 credit hrs)</div>
                    <div class="est-caption-th">Cost for Fall &amp; Spring Semesters <br />(min. 24 credit hrs)</div>
                  </div>
                </div>
                <div class="est-tbody">
                  <div class="est-tr">
                    <div class="est-th est-inner-border">University Tuition</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(tuitionAndFees/universityTuitionPerSemester, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(tuitionAndFees/universityTuitionPerSemester*2, '$##,###')" /></div>
                  </div>
                  <div class="est-tr est-tr-evenrow">
                    <div class="est-th est-inner-border">University Fees</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(tuitionAndFees/universityFeesPerSemester, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(tuitionAndFees/universityFeesPerSemester*2, '$##,###')" /></div>
                  </div>
                  <div class="est-tr">
                    <div class="est-th est-inner-border">College Tuition</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(tuitionAndFees/collegeTuitionPerSemester, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(tuitionAndFees/collegeTuitionPerSemester*2, '$##,###')" /></div>
                  </div>
                  <div class="est-tr est-tr-total table-bg__total totals">
                    <div class="est-th-total est-inner-border">Total Cost</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td-total est-inner-border"><xsl:value-of select="format-number(tuitionAndFees/totalPerSemester, '$##,###')" /></div>
                    <div class="est-td-total"><xsl:value-of select="format-number(tuitionAndFees/totalPerSemester*2, '$##,###')" /></div>
                  </div>
                </div>
              </div><!-- END .estimate-table -->
            </div><!-- END .wrap-table -->

            <!-- Scholarship Info -->
            <xsl:if test="scholarships/automaticAward = 'true'">
              <div class="wrap-table">
                <div class="est-table estimate-table responsive" id="estimate-table-scholarship">
                  <xsl:variable name="estimateTableScholarshipSubmissions">
                    <xsl:value-of select="queryOptions/gpa" /> GPA and <xsl:value-of select="queryOptions/score" />&#160;<xsl:if test="queryOptions/score > 100">SAT</xsl:if><xsl:if test="100 > queryOptions/score">ACT</xsl:if>
                  </xsl:variable>
                  <div class="est-caption table-bg__gold-dark">Automatically Awarded Scholarships<br /><xsl:value-of select="$estimateTableScholarshipSubmissions" /></div>
                  <div class="est-thead">
                    <div class="est-tr table-gold-bg table-bg__gold-medium">
                      <div class="est-caption-td est-inner-border">Scholarship Name</div>
                      <div class="est-caption-th est-inner-border">Value</div>
                      <div class="est-caption-th est-inner-border">Criteria for Consideration</div>
                      <div class="est-caption-th">How to Apply</div>
                    </div>
                  </div>
                  <div class="est-tbody">
                    <xsl:for-each select="scholarships">
                      <xsl:if test="automaticAward = 'true'">
                        <div id="cost-books" class="est-tr">
                          <div class="est-th est-inner-border"><xsl:value-of select="displayTitle" /></div>
                          <div class="est-td est-inner-border"><xsl:value-of select="displayValue" /></div>
                          <div class="est-td est-inner-border"><xsl:value-of select="displayCriteria" /></div>
                          <div class="est-td"><xsl:value-of select="displayHowToApply" /></div>
                        </div>
                      </xsl:if>
                    </xsl:for-each>
                  </div>
                </div><!-- END .estimate-table -->
              </div><!-- END .wrap-table -->
            </xsl:if>

            <!-- Scholarship Info -->
            <xsl:if test="scholarships/automaticAward = 'false'">
              <div class="wrap-table">
                <div class="est-table estimate-table responsive" id="estimate-table-scholarship">
                  <xsl:variable name="estimateTableScholarshipSubmissions">
                    <xsl:value-of select="queryOptions/gpa" /> GPA and <xsl:value-of select="queryOptions/score" />&#160;<xsl:if test="queryOptions/score > 100">SAT</xsl:if><xsl:if test="100 > queryOptions/score">ACT</xsl:if>
                  </xsl:variable>
                  <div class="est-caption table-bg__gold-dark">Possible Scholarship Opportunities<br /><xsl:value-of select="$estimateTableScholarshipSubmissions" /></div>
                  <div class="est-thead">
                    <div class="est-tr table-gold-bg table-bg__gold-medium">
                      <div class="est-caption-td est-inner-border">Scholarship Name</div>
                      <div class="est-caption-th est-inner-border">Value</div>
                      <div class="est-caption-th est-inner-border">Criteria for Consideration</div>
                      <div class="est-caption-th">How to Apply</div>
                    </div>
                  </div>
                  <div class="est-tbody">
                    <xsl:for-each select="scholarships">
                      <xsl:if test="automaticAward = 'false'">
                        <div id="cost-books" class="est-tr">
                          <div class="est-th est-inner-border"><xsl:value-of select="displayTitle" /></div>
                          <div class="est-td est-inner-border"><xsl:value-of select="displayValue" /></div>
                          <div class="est-td est-inner-border"><xsl:value-of select="displayCriteria" /></div>
                          <div class="est-td"><xsl:value-of select="displayHowToApply" /></div>
                        </div>
                      </xsl:if>
                    </xsl:for-each>
                  </div>
                </div><!-- END .estimate-table -->
              </div><!-- END .wrap-table -->
            </xsl:if>

            <!-- ADDITIONAL COSTS -->
            <div class="wrap-table">
              <div class="est-table estimate-table responsive" id="estimate-table-addi">
                <xsl:variable name="estimateTableAddiSubmissions">
                  <xsl:value-of select="translations/level" /> and <xsl:value-of select="translations/livingArrangement" />
                </xsl:variable>
                <div class="est-caption table-bg__gold-dark">Additional Costs<br /><xsl:value-of select="$estimateTableAddiSubmissions" /></div>
                <div class="est-thead">
                  <div class="est-tr table-gold-bg table-bg__gold-medium">
                    <div class="est-caption-td est-inner-border">&#160;<br />&#160;</div>
                    <div class="est-caption-th est-inner-border">Cost Per Credit <br />(1 credit hr)</div>
                    <div class="est-caption-th est-inner-border">Cost Per Semester <br />(min. 12 credit hrs)</div>
                    <div class="est-caption-th">Cost for Fall &amp; Spring Semesters <br />(min. 24 credit hrs)</div>
                  </div>
                </div>
                <div class="est-tbody">
                  <div id="cost-books" class="est-tr">
                    <div class="est-th est-inner-border">Books &amp; Supplies (approximately)</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(costOfLiving/booksSupplies, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(costOfLiving/booksSupplies*2, '$##,###')" /></div>
                  </div>
                  <div id="cost-room" class="est-tr table-bg__gold-light">
                    <div class="est-th est-inner-border">Room</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(costOfLiving/room, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(costOfLiving/room*2, '$##,###')" /></div>
                  </div>
                  <div id="cost-meals" class="est-tr">
                    <div class="est-th est-inner-border">Meals</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(costOfLiving/meals, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(costOfLiving/meals*2, '$##,###')" /></div>
                  </div>
                  <div id="cost-transportation" class="est-tr table-bg__gold-light">
                    <div class="est-th est-inner-border">Transportation</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(costOfLiving/transportationExpenses, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(costOfLiving/transportationExpenses*2, '$##,###')" /></div>
                  </div>
                  <div id="cost-personal-expense" class="est-tr">
                    <div class="est-th est-inner-border">Personal Expenses</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(costOfLiving/personalExpenses, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(costOfLiving/personalExpenses*2, '$##,###')" /></div>
                  </div>
                  <div class="est-tr table-bg__total">
                    <div class="est-th-total est-inner-border">Total Cost</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td-total est-inner-border"><xsl:value-of select="format-number(costOfLiving/totalPerSemester, '$##,###')" /></div>
                    <div class="est-td-total"><xsl:value-of select="format-number(costOfLiving/totalPerSemester*2, '$##,###')" /></div>
                  </div>
                </div>
              </div><!-- END .estimate-table -->
            </div><!-- END .wrap-table -->

            <!-- TOTAL COSTS - FALL / SPRING -->
            <div class="wrap-table">
              <div class="est-table estimate-table responsive" id="estimate-table-total" >
                <xsl:variable name="estimateTableTotalSubmissions">
                  <xsl:value-of select="translations/level" />, <xsl:value-of select="translations/semester" />, <xsl:value-of select="translations/residency" />, <xsl:value-of select="translations/livingArrangement" /> and <xsl:value-of select="major/title" />
                </xsl:variable>
                <div class="est-caption table-bg__green-dark">Total Estimated Cost of Attendance<br /><xsl:value-of select="$estimateTableTotalSubmissions" /></div>
                <div class="est-thead">
                  <div class="est-tr table-grey-bg table-bg__green-medium">
                    <div class="est-caption-td est-inner-border">&#160;<br />&#160;</div>
                    <div class="est-caption-th est-inner-border">Cost Per Credit <br />(1 credit hr)</div>
                    <div class="est-caption-th est-inner-border">Cost Per Semester <br />(min. 12 credit hrs)</div>
                    <div class="est-caption-th">Cost for Fall &amp; Spring Semesters <br />(min. 24 credit hrs)</div>
                  </div>
                </div>
                <div class="est-tbody">
                  <div class="est-tr">
                    <div class="est-th est-inner-border">Tuition &amp; Fees</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(tuitionAndFees/totalPerSemester, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(tuitionAndFees/totalPerSemester*2, '$##,###')" /></div>
                  </div>
                  <xsl:if test="scholarshipTotalAutomaticPerSemester > 0">
                    <div class="est-tr">
                      <div class="est-th est-inner-border">Automatic Scholarships</div>
                      <div class="est-td est-inner-border">-</div>
                      <div class="est-td est-inner-border">(<xsl:value-of select="format-number(scholarshipTotalAutomaticPerSemester, '$##,###')" />)</div>
                      <div class="est-td">(<xsl:value-of select="format-number(scholarshipTotalAutomaticPerSemester*2, '$##,###')" />)</div>
                    </div>
                  </xsl:if>
                  <div class="est-tr">
                    <div class="est-th est-inner-border">Additional Costs</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td est-inner-border"><xsl:value-of select="format-number(costOfLiving/totalPerSemester, '$##,###')" /></div>
                    <div class="est-td"><xsl:value-of select="format-number(costOfLiving/totalPerSemester*2, '$##,###')" /></div>
                  </div>
                  <div class="est-tr table-bg__total totals">
                    <div class="est-th-total est-inner-border">Total Cost</div>
                    <div class="est-td est-inner-border">-</div>
                    <div class="est-td-total est-inner-border"><xsl:value-of select="format-number(totalPerSemester, '$##,###')" /></div>
                    <div class="est-td-total"><xsl:value-of select="format-number(totalPerSemester*2, '$##,###')" /></div>
                  </div>
                </div>
              </div><!-- END .estimate-table -->
            </div><!-- END .wrap-table -->
          </section>

        </xsl:for-each>

      </xsl:when>

      <!-- if no entry through an error -->
      <xsl:otherwise>
        <xsl:choose>

          <!-- if there is an error message from lambda write it out -->
          <xsl:when test="message">

            <xsl:choose>
              <xsl:when test="code = '400BadRequest'">
                <h2>Validation Errors</h2>
                <p>The following questions need to be corrected to provide you with an estimate of your tuition and fees. Please <a href="">try again</a>.</p>
                <ul>
                  <xsl:for-each select="message/validationErrors">
                    <li> <xsl:value-of select="." /></li>
                  </xsl:for-each>
                </ul>
              </xsl:when>
              <xsl:when test="code = '404NotFound'">
                <h2>Not Found</h2>
                <p><xsl:value-of select="message" /></p>
              </xsl:when>
              <xsl:otherwise>
                <h2>Application Error</h2>
                <p><xsl:value-of select="message" /></p>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:when>

          <!-- didn't find the error so write a standard message out -->
          <xsl:otherwise>
            <p>There was an unknown error with the application.</p>
          </xsl:otherwise>

        </xsl:choose>
      </xsl:otherwise>

    </xsl:choose>

  </xsl:template>

</xsl:stylesheet>
