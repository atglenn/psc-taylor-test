<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/root">

    <xsl:choose>

      <!-- test if there is an entry -->
      <xsl:when test="entry">

        <xsl:for-each select="entry">

          <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
          <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

          <main id="maincontent" class="background main">

            <h2>Program Details</h2>
            <ul style="list-style: none;">
              <li><strong>Major Name:</strong>&#160;<xsl:value-of select="title" /></li>
              <xsl:if test="title != 'Undecided/Undeclared'" >
                <li><strong>Degree Program:</strong>&#160;<xsl:value-of select="degreeProgram/title" /></li>
                <xsl:variable name="degreeDesignationTitle"><xsl:value-of select="degreeDesignation/title" /></xsl:variable>
                <li><strong>Degree Designation:</strong>&#160;<abbr title="{$degreeDesignationTitle}"><xsl:value-of select="degreeDesignation/abbreviation" /></abbr></li>
                <li><strong>College / School:</strong>&#160;<xsl:value-of select="collegeOrSchool/title" /></li>
              </xsl:if>
            </ul>

            <xsl:if test="description">
              <xsl:value-of select="description" disable-output-escaping="yes" />
            </xsl:if>

            <xsl:if test="graduationRequirements">
              <h3>Graduation Requirements</h3>
              <xsl:value-of select="graduationRequirements" disable-output-escaping="yes" />
            </xsl:if>

            <xsl:if test="programOutcomes">
              <h3>Career Outcomes</h3>
              <xsl:value-of select="programOutcomes" disable-output-escaping="yes" />
            </xsl:if>

          </main>

          <aside class="wvu-sidebar">

            <h3>Recommended:</h3>
            <xsl:if test="advisementSheets">
              <xsl:variable name="fileURL"><xsl:value-of select="advisementSheets/file/url" /></xsl:variable>
              <a href="{$fileURL}">Two-Year Sequence Advisement Sheet</a>
            </xsl:if>

            <xsl:if test="programContacts">
              <h3>Program Contact</h3>
              <xsl:for-each select="programContacts">
                <xsl:sort select="contact/person/lastName"/>
                <p>
                  <xsl:variable name="emailAddress"><xsl:value-of select="contact/person/emailAddress" /></xsl:variable>
                  <strong><xsl:value-of select="contact/person/fullName" /></strong><br />
                  Email: <a href="mailto:{$emailAddress}"><xsl:value-of select="contact/person/emailAddress" /></a><br />
                  Phone: <xsl:value-of select="contact/person/phoneNumber" />
                  <xsl:choose>
                    <xsl:when test="contact/person/phoneExtension">
                      ext. <xsl:value-of select="contact/person/phoneExtension" />
                    </xsl:when>
                  </xsl:choose>
                </p>
              </xsl:for-each>
            </xsl:if>

            <h3>Tuition and Fees</h3>
            <ul>
              <xsl:for-each select="tuitionAndFees">
                <xsl:if test="year/yearTwoDigits = '18'" >
                  <li><strong>Resident of WV:</strong>&#160;<xsl:value-of select="format-number(totalPerSemesterResident*2, '$##,###')" /></li>
                  <li><strong>Metro:</strong>&#160;<xsl:value-of select="format-number(totalPerSemesterMetroResident*2, '$##,###')" /></li>
                  <li><strong>Non-Resident:</strong>&#160;<xsl:value-of select="format-number(totalPerSemesterNonResident*2, '$##,###')" /></li>
                </xsl:if>
              </xsl:for-each>
            </ul>

          </aside>

        </xsl:for-each>

      </xsl:when>

      <!-- if no entry through an error -->
      <xsl:otherwise>
        <xsl:choose>

          <!-- if there is an error message from lambda write it out -->
          <xsl:when test="message">

            <xsl:choose>
              <xsl:when test="code = '404NotFound'">
                <h2>No Results</h2>
              </xsl:when>
              <xsl:otherwise>
                <h2>Application Error</h2>
              </xsl:otherwise>
            </xsl:choose>

            <p><xsl:value-of select="message" /></p>

          </xsl:when>

          <!-- didn't find the error so write a standard message out -->
          <xsl:otherwise>
            <p>There was an unknown error with the application.</p>
          </xsl:otherwise>

        </xsl:choose>
      </xsl:otherwise>

    </xsl:choose>

  </xsl:template>
</xsl:stylesheet>
