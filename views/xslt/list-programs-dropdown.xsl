<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/root">
    <xsl:choose>

      <!-- test if there is an entry -->
      <xsl:when test="entry">

        <xsl:for-each select="entry">
          <xsl:sort select="title" />
          <xsl:variable name="majorKey"><xsl:value-of select="majorKey" /></xsl:variable>
          <option value="{$majorKey}"><xsl:value-of select="title" /> (<xsl:value-of select="degreeDesignation/abbreviation" />)</option>
        </xsl:for-each>

      </xsl:when>

      <!-- if no entry through an error -->
      <xsl:otherwise>
        <xsl:choose>

          <!-- if there is an error message from lambda write it out -->
          <xsl:when test="message">

            <xsl:choose>
              <xsl:when test="code = '404NotFound'">
                <h2>No Results</h2>
              </xsl:when>
              <xsl:otherwise>
                <h2>Application Error</h2>
              </xsl:otherwise>
            </xsl:choose>

            <p><xsl:value-of select="message" /></p>

          </xsl:when>

          <!-- didn't find the error so write a standard message out -->
          <xsl:otherwise>
            <p>There was an unknown error with the application.</p>
          </xsl:otherwise>

        </xsl:choose>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
