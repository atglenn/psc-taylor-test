$(document).ready(function(){
  var submitIcon = $('.psc-search-icon');
  var inputBox = $('.psc-search-input');
  var searchBox = $('.psc-search');
  var isOpen = false;
  submitIcon.click(function(){
   if(isOpen == false){
    $('.psc-search-input').css('visibility','visible');
    searchBox.addClass('psc-search-open');
    inputBox.focus();
    isOpen = true;
   } else {
    searchBox.removeClass('psc-search-open');
    $('.psc-search-input').css('visibility','hidden');
    inputBox.focusout();
    isOpen = false;
   }
  });
   submitIcon.mouseup(function(){
    return false;
   });
   searchBox.mouseup(function(){
    return false;
   });
  $(document).mouseup(function(){
   if(isOpen == true){
    $('.psc-search-icon').css('display','block');
    submitIcon.click();
  }
});
 });
function buttonUp(){
 var inputVal = $('.psc-search-input').val();
 inputVal = $.trim(inputVal).length;
 if( inputVal !== 0){
  $('.psc-search-icon').css('display','none');
 } else {
  $('.psc-search-input').val('');
  $('.psc-search-icon').css('display','block');
 }
}
